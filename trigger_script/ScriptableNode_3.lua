--You can use 'params.parameter name' to get the parameters defined in the node. 					
--For example, if a parameter named 'entity' is defined in the node, you can use 'params.entity' to get the value of the parameter.
Trigger.RegisterHandler(World.cfg, "GAME_START", function()
    local players = Game.GetAllPlayers()
    for i, v in pairs(players) do
        v:setPos(Lib.v3(0, 0, 0), 0, 0)
    end
end)